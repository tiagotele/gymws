package com.tiago.hello.com.tiago.hello.model;

/**
 * Created by Tiago on 28/02/17.
 */
public class Equipment {
    private String name;
    private Integer machineNumber;

    public Equipment() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMachineNumber() {
        return machineNumber;
    }

    public void setMachineNumber(Integer machineNumber) {
        this.machineNumber = machineNumber;
    }
}
