package com.tiago.hello.com.tiago.hello.model;

/**
 * Created by Tiago on 28/02/17.
 */
public enum Goal {
    BODY_BUILDING("Body building"),
    LOSE_WEIGHT("Lose weight"),
    KEEP_BODY("Keep body");

    private final String goal;

    private Goal(final String goal) {
        this.goal = goal;
    }

    @Override
    public String toString() {
        return this.goal;
    }
}
