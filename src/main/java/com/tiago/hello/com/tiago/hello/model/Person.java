package com.tiago.hello.com.tiago.hello.model;

/**
 * Created by Tiago on 28/02/17.
 */
public class Person {
    private String name;
    private Integer age;
    private Goal goal;

    public Person() {
        this.name = "Tiago Barbosa Melo";
        this.age = 30;
        this.goal = Goal.BODY_BUILDING;
    }

    public Person(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Goal getGoal() {
        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }

}
