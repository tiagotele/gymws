package com.tiago.hello.com.tiago.hello.controller;

import com.tiago.hello.com.tiago.hello.model.Equipment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class EquipamentController {

    private static final String path = "/equipment";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = path, method = RequestMethod.GET)
    public
    @ResponseBody
    Equipment get(
            @RequestParam(value = "id", required = true, defaultValue = "1") Long id) {
        logger.info("EquipamentController get");
        Equipment equipment = new Equipment();
        equipment.setMachineNumber(30);
        equipment.setName("supino");
        return equipment;
    }

    @RequestMapping(value = path, method = RequestMethod.DELETE)
    public
    @ResponseBody
    Equipment delete(
            @RequestParam(value = "id", required = true, defaultValue = "1") Long id) {
        logger.info("EquipamentController delete");
        Equipment equipment = new Equipment();
        equipment.setMachineNumber(40);
        equipment.setName("rosca scott");
        return equipment;
    }

    @RequestMapping(value = path, method = RequestMethod.POST)
    public
    @ResponseBody
    Equipment add(
            @RequestBody Equipment equipment) {
        logger.info("EquipamentController post");
        return equipment;
    }

    @RequestMapping(value = path, method = RequestMethod.PUT)
    public
    @ResponseBody
    Equipment edit(
            @RequestBody Equipment equipment) {
        logger.info("EquipamentController put");
        return equipment;
    }
}
