package com.tiago.hello.com.tiago.hello.model;

import java.util.Set;

/**
 * Created by Tiago on 28/02/17.
 */
public class TrainningPlan {
    Long id;
    Set<TrainningGroup> trainningGroups;
    Person person;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<TrainningGroup> getTrainningGroups() {
        return trainningGroups;
    }

    public void setTrainningGroups(Set<TrainningGroup> trainningGroups) {
        this.trainningGroups = trainningGroups;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
