package com.tiago.hello.com.tiago.hello.controller;

import com.tiago.hello.com.tiago.hello.model.Equipment;
import com.tiago.hello.com.tiago.hello.model.Exercise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class ExerciseController {

    private static final String path = "/exercise";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = path, method = RequestMethod.GET)
    public
    @ResponseBody
    Exercise get(
            @RequestParam(value = "id", required = true, defaultValue = "1") Long id) {

        logger.info("ExerciseController get");

        Equipment equipment = new Equipment();
        equipment.setMachineNumber(10);
        equipment.setName("supino");

        Exercise exercise = new Exercise();
        exercise.setName("on get");
        exercise.setTarget("target");
        exercise.setEquipment(equipment);

        return exercise;
    }

    @RequestMapping(value = path, method = RequestMethod.DELETE)
    public
    @ResponseBody
    String delete(
            @RequestParam(value = "id", required = false, defaultValue = "1") Long id) {

        logger.info("ExerciseController delete");

        return "deleted";
    }

    @RequestMapping(value = path, method = RequestMethod.POST)
    public
    @ResponseBody
    Exercise add(
            @RequestBody Exercise exercise) {

        logger.info("ExerciseController add");

        return exercise;
    }

    @RequestMapping(value = path, method = RequestMethod.PUT)
    public
    @ResponseBody
    Exercise edit(
            @RequestBody Exercise exercise) {

        logger.info("ExerciseController put");

        return exercise;
    }
}
