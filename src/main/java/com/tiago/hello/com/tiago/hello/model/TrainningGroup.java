package com.tiago.hello.com.tiago.hello.model;

import java.util.Set;

/**
 * Created by Tiago on 28/02/17.
 */
public class TrainningGroup {
    Long id;
    Set<Exercise> exercisesSet;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Exercise> getExercisesSet() {
        return exercisesSet;
    }

    public void setExercisesSet(Set<Exercise> exercisesSet) {
        this.exercisesSet = exercisesSet;
    }
}
