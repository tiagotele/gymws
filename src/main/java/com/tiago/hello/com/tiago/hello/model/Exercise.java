package com.tiago.hello.com.tiago.hello.model;

/**
 * Created by Tiago on 28/02/17.
 */
public class Exercise {
    private String name;
    private String target;
    private Equipment equipment;

    public Exercise() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }
}
