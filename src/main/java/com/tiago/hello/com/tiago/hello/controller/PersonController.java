package com.tiago.hello.com.tiago.hello.controller;

import com.tiago.hello.com.tiago.hello.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
public class PersonController {

    private static final String path = "/person";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = path, method = RequestMethod.GET)
    public
    @ResponseBody
    Person get(@RequestParam(value = "id", required = true, defaultValue = "1") Long id) {

        logger.info("PersonController get");

        Person person = new Person();
        person.setAge(31);
        person.setName("Tiago Barbosa Melo");
        return person;
    }

    @RequestMapping(value = path, method = RequestMethod.DELETE)
    public
    @ResponseBody
    String delete(@RequestParam(value = "id", required = true, defaultValue = "1") Long id) {
        logger.info("PersonController delete");
        return "Deleted";
    }

    @RequestMapping(value = path, method = RequestMethod.POST)
    public
    @ResponseBody
    Person add(@RequestBody Person person) {
        logger.info("PersonController post");
        return person;
    }

    @RequestMapping(value = path, method = RequestMethod.PUT)
    public
    @ResponseBody
    Person edit(@RequestBody Person person) {
        logger.info("PersonController put");
        return person;
    }

}
